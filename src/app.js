import React, { Component } from 'react';

import AuthorizationContainer from './containers/authorization/authorizationContainer';

export default class App extends Component {
  render() {
    return (
      <div id="root">
          <AuthorizationContainer/>
      </div>
    );
  }
}
