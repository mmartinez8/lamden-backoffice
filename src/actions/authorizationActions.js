import {AUTHORIZATION} from "./types";

export const login = () =>{
    return {
        type: AUTHORIZATION,
        isLoggedIn: true,
        authorizationData: "Usuario logueado!"
    }
}