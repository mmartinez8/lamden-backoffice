import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {login} from "../../actions/authorizationActions"
import { BrowserRouter as Router } from 'react-router-dom'

class AuthorizationContainer extends Component{

    render =()=>
        <div>
              <button onClick={this.onLoginClick}>Login </button>
        </div>

    onLoginClick = () =>{
        this.props.login();
        this.props.history.push('/userDashboard');
    }

}

const mapStateToProps = (state) => {
    return {authorizationData: state.authorizationData}
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({login: login}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationContainer);