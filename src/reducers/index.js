import { combineReducers } from 'redux';
import authorizationReducer from "./authorizationReducer";

const rootReducer = combineReducers({
    state: (state = {}) => state,
    authorizationReducer: authorizationReducer
});

export default rootReducer;
