import createReducer from './helpers/reducerHelper'
import {AUTHORIZATION} from "../actions/types";

const initialState = {
    isLoggedIn: false,
    authorizationData: {},
};

const authorizationReducer = createReducer(initialState,
    {

        [AUTHORIZATION](state, action) {
            console.log("Log in action");
            return {
                ...state,
                isLoggedIn: true,
                authorizationData: "Hola Usuario",
            };
        }


    });

export default authorizationReducer;