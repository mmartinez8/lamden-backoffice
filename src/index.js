import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {BrowserRouter, Route } from "react-router-dom";
import AuthorizationContainer from "./containers/authorization/authorizationContainer";
import UserDashboardContainer from "./containers/userDashboard/userDashboardContainer";


import App from './app';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
      <BrowserRouter>
          <div>
              <Route path="/login" component={AuthorizationContainer} />
              <Route path="/userDashboard" component={UserDashboardContainer} />
          </div>
      </BrowserRouter>
  </Provider>
  , document.getElementById('root'));
